# groupator

Making groups of groups.

## Example
### Input

A list of elements. 

```
William 'Bill' Blake
Xebeche
John Scholfield
John Dickinson
Cole Wilson
Conway Twill
Johnny 'The Kid' Pickett
Salvatore 'Sally' Jenko
Big George Drakoulious
Thel Russell
Benmont Tench
Charles Ludlow 'Charlie' Dickinson
M. Olafsen
```

### Output

Sets of groups of two elements such that every group appears only once in all
the sets (i.e. two elements can be grouped together only once).

```
-> set 1
William 'Bill' Blake        Xebeche
John Scholfield             M. Olafsen
John Dickinson              Charles Ludlow 'Charlie' Dickinson
Cole Wilson                 Benmont Tench
Conway Twill                Thel Russell
Johnny 'The Kid' Pickett    Big George Drakoulious
Salvatore 'Sally' Jenko     nobody
 
-> set 2
William 'Bill' Blake        John Scholfield
Xebeche                     nobody
John Dickinson              M. Olafsen
Cole Wilson                 Charles Ludlow 'Charlie' Dickinson
Conway Twill                Benmont Tench
Johnny 'The Kid' Pickett    Thel Russell
Salvatore 'Sally' Jenko     Big George Drakoulious
 
-> set 3
...
```

