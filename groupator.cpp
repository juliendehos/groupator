// g++ -Wall -std=c++11 groupator.cpp
// ./a.out < input.txt > output.txt
#include <boost/multi_array.hpp>
#include <iostream>
#include <string>
#include <vector>
int main(int argc, char ** argv) {
  std::string student;
  std::vector<std::string> tabStudents;
  while ( std::getline(std::cin, student) ) tabStudents.push_back(student);
  if (tabStudents.size() % 2 == 1) tabStudents.push_back("nobody");
  int N = tabStudents.size();
  int N1 = N-1;
  boost::multi_array<int, 2> M;
  M.resize(boost::extents[N][N]);
  for (int i=0; i<N; i++) 
    for (int j=i+1; j<N1; j++) 
      M[j][i] = M[i][j] = (i+j+N1-1)%N1 + 1;
  for (int i=1; i<N; i++) M[i][N1] = (2*i+N1-1)%N1 + 1;
  for (int t=1; t<=N1; t++) {
    std::cout << "\n-> Session " << t << " =\n";
    for (int i=0; i<N; i++)
      for (int j=i+1; j<N; j++)	 
	if (M[i][j] == t)
	  std::cout << tabStudents[i] << "\t" << tabStudents[j] << std::endl;
  }
  std::cout << std::endl;
  return 0;
}
