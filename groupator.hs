-- runhaskell groupator.hs < input.txt > output.txt
import Data.List
-- hyp: i < j
calcIndex n1 i j = if j==n1 then (2*i+n1-1)`mod`n1 + 1 else (i+j+n1-1)`mod`n1 + 1
calcSessions n1 = sortBy f ss
    where ss = [(calcIndex n1 e1 e2,e1,e2) | e1<-[0..n1], e2<-[(e1+1)..n1]]
          f (s1,_,_) (s2,_,_) = s1 `compare` s2
calcStr es = map f ss
    where es' = if even (length es) then es else es++["nobody"]
          ss = calcSessions $ length es' - 1
          getNom e = es' !! e
          f (s, e1, e2) = show s ++ "\t" ++ getNom e1 ++ "\t" ++ getNom e2 ++ "\n"
main = do 
  content <- getContents
  mapM_ putStr $ calcStr $ lines content
