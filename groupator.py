# python2 groupator.py < input.txt > output.txt
import numpy, sys
# get names
names = [line.split('\n')[0] for line in sys.stdin]
if len(names) % 2 == 1 :
    names.extend(["nobody"])
N = len(names)
N1 = N-1
# build matrix
M = numpy.empty((N, N), dtype=int)
for i in range(N) :
    for j in range(N):
        M[i][j] = (i+j+N1-1)%N1 + 1
for i in range(N) :
    M[N1][i] = M[i][N1] = (2*i+N-2)%N1 + 1
for i in range(N) :
    M[i][i] = 0
# display results
for k in range(1,N) :
    print "-> TP", k
    for i in range(N) :
        for j in range(i,N) :
            if M[i][j] == k :
                print names[i], "\t", names[j]
    print " "
print M
