-- runhaskell groupator1_slow.hs < input.txt > output.txt

import Control.Monad

calculerBinomes es = [(e1,e2) | e1<-es, e2<-es, e1/=e2, e1<e2]

estPresent (e1,e2) (e1',e2') = e1==e1' || e1==e2' || e2==e1' || e2==e2'

estValide (x:xs) = not (any (estPresent x) xs) && estValide xs
estValide _ = True

calculerSessions es = take (n - 1) $ filter estValide comb
    where es' = if even (length es) then es else "nobody":es
          n = length es'
          comb = replicateM (n `div` 2) (calculerBinomes es')
          --comb = (sequence . replicate (n `div` 2)) (calculerBinomes es')
         
main = do 
  content <- getContents
  let sessions = calculerSessions $ lines content
      formatBinome (e1, e2) = e1 ++ "\t" ++ e2
    in putStr $ (unlines . map (unlines . map formatBinome)) sessions
