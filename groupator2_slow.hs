-- runhaskell groupator2_slow.hs < input.txt > output.txt

import Data.List

coupler (x1:x2:xs) = (x1,x2) : coupler xs
coupler _ = []

estValide ((e1,e2):xs) ss = (e2,e1) `notElem` ss && estValide xs ss
estValide _ _ = True

calculerSessions es = take (length es' - 1) $ foldl f [] comb
    where es' = if even (length es) then es else "nobody":es
          comb = map coupler $ permutations es'
          f acc s = if all (estValide s) acc then s:acc else acc

main = do 
  content <- getContents
  let sessions = calculerSessions $ lines content
      formatBinome (e1, e2) = e1 ++ "\t" ++ e2
    in putStr $ (unlines . map (unlines . map formatBinome)) sessions
