// g++ -Wall -std=c++11  groupator_test.cpp

#include <boost/multi_array.hpp>
#include <iostream>
#include <vector>

std::ostream & operator << (std::ostream & os, boost::multi_array<int, 2>& M) {
  int N = M.size();
  for (int i=0; i<N; i++) {
      for (int j=0; j<N; j++) {
	  os << M[i][j] << " ";
	}
      os << std::endl;
    }
  return os;
}

void checkMatrix(boost::multi_array<int, 2>& M) {
  int N = M.size();
  // verifie diagonale nulle
  for (int i=0; i<N; i++) {
      if (M[i][i] != 0) throw std::string("error in checkMatrix : diagonale");
    }
  // verifie coefs intra-lignes differents
  for (int t=0; t<N; t++) {
      for (int i=0; i<N; i++) {
	  int s = 0;
	  for (int j=0; j<N; j++) 
	      if (M[i][j] == t) s++;
	  if (s != 1) throw std::string("error in checkMatrix : coefs intra-lignes");
	}
    }
  // verifie coefs intra-colonnes differents
  for (int t=0; t<N; t++) {
      for (int j=0; j<N; j++) {
	  int s = 0;
	  for (int i=0; i<N; i++) 
	      if (M[i][j] == t) s++;
	  if (s != 1) throw std::string("error in checkMatrix : coefs intra-colonnes");
	}
    }
  // verifie matrice symetrique
  for (int i=0; i<N; i++) {
      for (int j=0; j<N; j++) 
	  if (M[i][j] != M[j][i]) throw std::string("error in checkMatrix : symetrie");
    }
}

// input : Np (number of students)
// ouput : M (matrix of sessions for student pairs) 
// ouput : N (smallest even integer > Np) 
void initMatrix(const int Np, boost::multi_array<int, 2>& M, int & N, int & N1) {
  // test Np
  if (Np<4)
    throw std::string("error in init : N must be greater than 4");
  // compute N
  N = ((Np+1)>>1)<<1;
  N1 = N - 1;
  // compute M
  M.resize(boost::extents[N][N]);
  for (int i=0; i<N; i++) 
      for (int j=i+1; j<N; j++) 
	  M[j][i] = M[i][j] = (i+j+N1-1)%N1 + 1;
  //for (int i=0; i<N; i++) M[N1][i] = M[i][N1] = M[i][i];
  for (int i=1; i<N1; i++) M[N1][i] = M[i][N1] = (2*i+N1-1)%N1 + 1;
  for (int i=0; i<N; i++) M[i][i] = 0;
}

int main() {

  try {
    for (int n=4; n<=20; n+=2) {
      boost::multi_array<int, 2> M;
      int N, N1;
      initMatrix(n, M, N, N1); 
      checkMatrix(M);
      std::cerr << M << std::endl;
    }
  }
  catch (const std::string & s) {
    std::cerr << s << std::endl;
  }

  return 0;
}
